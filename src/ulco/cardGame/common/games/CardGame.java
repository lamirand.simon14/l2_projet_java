package ulco.cardGame.common.games;

import ulco.cardGame.common.games.boards.CardBoard;
import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Player;

import java.io.*;
import java.net.Socket;
import java.util.*;

/**
 * CardGame which extend BoardGame
 * BoardGame need to implement Game interface methods
 */
public class CardGame extends BoardGame {

    private List<Card> cards;
    private Integer nbRounds;

    /**
     * Enable constructor of CardGame
     * - Name of the game
     * - Maximum number of players of the Game
     *  - Filename of current Game
     * @param name
     * @param maxPlayers
     * @param filename
     */
    public CardGame(String name, Integer maxPlayers, String filename) {
        super(name, maxPlayers, filename);

        this.board = new CardBoard();
        this.nbRounds = 0;
    }

    @Override
    public void initialize(String filename) {

        this.cards = new ArrayList<>();

        // Initialsiation de la liste des cartes
        try {
            File cardFile = new File(filename);
            Scanner reader = new Scanner(cardFile);

            while (reader.hasNextLine()) {

                String data = reader.nextLine();
                String[] dataValues = data.split(";");

                // get Card value
                Integer value = Integer.valueOf(dataValues[1]);
                this.cards.add(new Card(dataValues[0], value, true));
            }
            reader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    @Override
    public Player run() {

        Player gameWinner = null;

        // distribue les cartes à chaque joueur
        Collections.shuffle(cards);

        int playerInd = 0;

        for (Card card : cards) {

            players.get(playerInd).addComponent(card);
            card.setPlayer(players.get(playerInd));

            playerInd++;

            if (playerInd >= players.size()) {
                playerInd = 0;
            }
        }

        //Envoi des maj de l'état du jeu pour chaque joueur
        for (Player player : players) {
            System.out.println(player.getName() + " has " + player.getComponents().size() + " cards");
        }

        // tant que chaque joueur peut jouer
        while (!this.end()) {

            Map<Player, Card> playedCard = new HashMap<>();

            for (Player player : players) {

                if (!player.isPlaying())
                    continue;
                
                //carte jouée par le joueur actuel
                Card card = (Card) player.play();

                // ajoute la carte sur le board
                board.addComponent(card);
                
                playedCard.put(player, card);

                System.out.println(player.getName() + " has played " + card.getName());
            }

          
            board.displayState();

            // permet de connaître quel joueur gagne le round
            int bestValueCard = 0;

            List<Player> gagnantProbable = new ArrayList<>();
            List<Card> gagnantProbableCards = new ArrayList<>();

            for (Card card : playedCard.values()) {
                if (card.getValue() >= bestValueCard)
                    bestValueCard = card.getValue();
            }

            // si égalité
            for(Map.Entry<Player, Card> entry : playedCard.entrySet()){

                Card currentCard = entry.getValue();

                if (currentCard.getValue() >= bestValueCard) {

                    gagnantProbable.add(entry.getKey());
                    gagnantProbableCards.add(currentCard);
                }
            }

            
            int winnerIndex = 0;

            // Victoire au hasard si égalité
            if (gagnantProbable.size() > 1){
                Random random = new Random();
                winnerIndex = random.nextInt(gagnantProbable.size() - 1);
            }

            Player gagnantRound = gagnantProbable.get(winnerIndex);
            Card winnerCard = gagnantProbableCards.get(winnerIndex);

            System.out.println("Player " + gagnantRound + " won the round with " + winnerCard);

            // MAJ état du jeu
            for (Card card : playedCard.values()) {

                gagnantRound.addComponent(card);
                card.setPlayer(gagnantRound);
            }


            board.clear();

            // Vérifie l'état des joueurs
            for (Player player : players){

                // Un joueur ne peut plus jouer si il n'a plus de carte dans sa main
                if (player.getScore() == 0)
                    player.canPlay(false);

                // Un joueur gagne si il a toutes les cartes du jeu dans sa main
                if (player.getScore() == cards.size()) {
                    player.canPlay(false);
                    gameWinner = player;
                }
            }


            this.displayState();

            // mélange la main des joueurs tous les n rounds
            this.nbRounds += 1;

            if (this.nbRounds % 10 == 0) {
                for (Player player : players){
                    player.shuffleHand();
                }
            }
        }

        return gameWinner;
    }

    public Player run(Map<Player, Socket> playerSockets) throws IOException, ClassNotFoundException {

        for (Socket playerSocket : playerSockets.values()) {
            ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
            playerOos.writeObject("Game start");
        }

        Player gameWinner = null;

        // Distribution des cartes à chaque joueur
        Collections.shuffle(cards);

        int playerInd = 0;

        for (Card card : cards) {

            players.get(playerInd).addComponent(card);
            card.setPlayer(players.get(playerInd));

            playerInd++;

            if (playerInd >= players.size()) {
                playerInd = 0;
            }
        }

        for (Socket playerSocket : playerSockets.values()) {
            ObjectOutputStream playerGame = new ObjectOutputStream(playerSocket.getOutputStream());
            playerGame.writeObject(this);
        }


        // envoit l'état du jeu à chaque joueur

        for(Map.Entry<Player,Socket>entry : playerSockets.entrySet()){
            ObjectOutputStream playercards = new ObjectOutputStream(entry.getValue().getOutputStream());
            playercards.writeObject("You have " + entry.getKey().getComponents().size() + "cards");
        }


        // tant que chaque joueur peut jouer
        while (!this.end()) {

            Map<Player, Card> playedCard = new HashMap<>();

            for (Player player : players) {

                if (!player.isPlaying())
                    continue;

                // Carte jouée pour chaque joueur
                String msg = "[" + player.getName() + "] is your turn to play";
                ObjectOutputStream sendmsg = new ObjectOutputStream(playerSockets.get(player).getOutputStream());
                sendmsg.writeObject(msg);

                System.out.println("Waiting for " + player.getName() + " to play...");

                for(Map.Entry<Player,Socket>entry : playerSockets.entrySet()){
                    if(entry.getKey()!=player){
                        ObjectOutputStream Oos = new ObjectOutputStream(entry.getValue().getOutputStream());
                        Oos.writeObject("Waiting for " + player.getName() + " to play...");
                    }
                }

                player.play(playerSockets.get(player));

                Object answer;
                Card card = null;

                do {

                    ObjectInputStream ois = new ObjectInputStream(playerSockets.get(player).getInputStream());
                    answer = ois.readObject();
                    if (answer instanceof Card)
                        card = (Card)answer;
                } while (!(answer instanceof Card));
                ObjectOutputStream oos3 = new ObjectOutputStream(playerSockets.get(player).getOutputStream());
                oos3.writeObject("You played " + card.getName());
//                pour chaque carte du jeu:
//                si valeur_carte_client == valeur_carte_jeu
//                supprimer la carte du jeu

                for(Component card1 : player.getSpecificComponents(Card.class)){
                    if(card1.getId().equals(card.getId())){
                        player.removeComponent(card1);
                    }
                }

                board.addComponent(card);

                playedCard.put(player, card);

                System.out.println(player.getName() + " has played " + card.getName());
            }

            for(Map.Entry<Player,Socket>entry : playerSockets.entrySet()){
                ObjectOutputStream sendboard = new ObjectOutputStream(entry.getValue().getOutputStream());
                sendboard.writeObject(board);
            }

            // Vérifie quel joueur gagne
            int bestValueCard = 0;

            List<Player> gagnantProbable = new ArrayList<>();
            List<Card> gagnantProbableCards = new ArrayList<>();

            for (Card card : playedCard.values()) {
                if (card.getValue() >= bestValueCard)
                    bestValueCard = card.getValue();
            }

            // Si égalité
            for(Map.Entry<Player, Card> entry : playedCard.entrySet()){

                Card currentCard = entry.getValue();

                if (currentCard.getValue() >= bestValueCard) {

                    gagnantProbable.add(entry.getKey());
                    gagnantProbableCards.add(currentCard);
                }
            }

            // default winner index
            int winnerIndex = 0;

            // Random choice if equality is reached
            if (gagnantProbable.size() > 1){
                Random random = new Random();
                winnerIndex = random.nextInt(gagnantProbable.size() - 1);
            }

            Player gagnantRound = gagnantProbable.get(winnerIndex);
            Card winnerCard = gagnantProbableCards.get(winnerIndex);

            for(Map.Entry<Player,Socket>entry : playerSockets.entrySet()){
                ObjectOutputStream sendWinner = new ObjectOutputStream(entry.getValue().getOutputStream());
                sendWinner.writeObject("Player " + gagnantRound + " won the round with " + winnerCard);
            }

            System.out.println("Player " + gagnantRound + " won the round with " + winnerCard);

            // Update Game state
            for (Card card : playedCard.values()) {

                // remove from previous player
                gagnantRound.addComponent(card);
                card.setPlayer(gagnantRound);
            }


            board.clear();

            // Vérifie l'état des joueurs
            for (Player player : players){

                // Les joueurs ne peuvent plus jouer si aucune carte dans leur main
                if (player.getScore() == 0){
                    player.canPlay(false);
                    ObjectOutputStream sendLose = new ObjectOutputStream(playerSockets.get(player).getOutputStream());
                    sendLose.writeObject("YOU LOSE !!! (you can't play so :-( )");
                }


                // Un joueur gagne quand il a toutes les cartes dans sa main
                if (player.getScore() == cards.size()) {
                    player.canPlay(false);
                    ObjectOutputStream sendWin = new ObjectOutputStream(playerSockets.get(player).getOutputStream());
                    sendWin.writeObject("YOU WIN !!!");
                    gameWinner = player;
                }
            }

            // mélange la main des joueurs tous les n rounds
            this.nbRounds += 1;

            if (this.nbRounds % 10 == 0) {
                for (Player player : players){
                    player.shuffleHand();
                }
            }

            for(Map.Entry<Player,Socket>entry : playerSockets.entrySet()){
                ObjectOutputStream updateGame = new ObjectOutputStream(entry.getValue().getOutputStream());
                updateGame.writeObject(this);
            }
        }
        return gameWinner;
    }

    @Override
    public boolean end() {
        // Vérifie si c'est la fin de la partie
        endGame = true;
        for (Player player : players) {
            if (player.isPlaying()) {
                endGame = false;
            }
        }

        return endGame;
    }

    @Override
    public String toString() {
        return "CardGame{" +
                "name='" + name + '\'' +
                '}';
    }
}
