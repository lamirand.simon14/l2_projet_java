package ulco.cardGame.common.games;

import ulco.cardGame.common.games.boards.PokerBoard;
import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Coin;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Player;

import java.io.*;
import java.net.Socket;
import java.util.*;

public class PokerGame extends BoardGame {

    private List<Card> cards;
    private List<Coin> coins;
    private Integer maxRounds;
    private Integer numberOfRounds;

    /**
     * Enable constructor of Game
     * - Name of the game
     * - Maximum number of players of the Game
     * - Expected number of rounds until end
     * - Filename with all required information to load the Game
     *
     * @param name
     * @param maxPlayers
     * @param maxRounds
     * @param filename
     */
    public PokerGame(String name, Integer maxPlayers, Integer maxRounds, String filename) {
        super(name, maxPlayers, filename);

        this.maxRounds = maxRounds;
        this.numberOfRounds = 0;
        this.board = new PokerBoard();
    }

    @Override
    public void initialize(String filename) {

        this.cards = new ArrayList<>();
        this.coins = new ArrayList<>();

        // Initialisation de la liste des cartes
        try {
            File cardFile = new File(filename);
            Scanner myReader = new Scanner(cardFile);

            while (myReader.hasNextLine()) {

                String data = myReader.nextLine();
                String[] dataValues = data.split(";");

                // Extrait des informations à partir du fichier
                String className = dataValues[0];
                String componentName = dataValues[1];
                Integer componentValue = Integer.valueOf(dataValues[2]);


                if (className.equals(Card.class.getSimpleName())) {

                    this.cards.add(new Card(componentName, componentValue, true));
                }
                else if (className.equals(Coin.class.getSimpleName())){

                    // Ajoute ce jeton pour chaque joueur possible dans le Poker
                    for (int i = 0; i < this.maxNumberOfPlayers(); i++) {

                        this.coins.add(new Coin(componentName, componentValue));
                    }
                }
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    @Override
    public Player run() {

        Player gameWinner = null;

        // Distribue chaque jeton
        int playerIndex = 0;

        for (Coin coin : coins) {

            players.get(playerIndex).addComponent(coin);
            coin.setPlayer(players.get(playerIndex));

            playerIndex++;

            if (playerIndex >= players.size()) {
                playerIndex = 0;
            }
        }

        do {
            // distribuer les cartes à chaque joueur pour ce round
            Collections.shuffle(cards);

            // Distribue 3 cartes à chaque joueur
            int cardIndex = 0;

            for (int i = 0; i < 3; i++) {

                for (Player player : players) {
                    player.addComponent(cards.get(cardIndex));
                    cards.get(cardIndex).setPlayer(player);
                    cardIndex++;
                }
            }


            // En fonction de la du joueur, il peut choisir un jeton à jouer
            for (Player player : players) {
                board.addComponent(player.play());
            }

            // Affichage de 3 cartes
            for (int i = 0; i < 3; i++) {
                board.addComponent(cards.get(cardIndex));
                cardIndex++;
            }

            board.displayState();

            // En fonction de la main des joueurs et de l'état du jeu, les joueurs peuvent choisir un jeton à jouer
            for (Player player : players) {
                board.addComponent(player.play());
            }
            // Le gagnant est le joueur avec le plus carte de même valeur et de plus haut rang

            //Stock le joueur qui a la meilleure carte
            Map<Player, Map.Entry<Integer, Integer>> usersCardValues = new HashMap<>();

            for (Player player : players) {

                // Recupere toutes les cartes
                List<Component> allCards = new ArrayList<>();
                allCards.addAll(player.getSpecificComponents(Card.class));
                allCards.addAll(board.getSpecificComponents(Card.class));

                Map<Integer, Integer> counts = new HashMap<>();


                for (Component e : allCards) {
                    counts.merge(e.getValue(), 1, Integer::sum);
                }

                // Obtient le nb d'occurence max
                Optional<Integer> maxOccurrences = counts.entrySet().stream() // Get stream of all counts
                        .max((e1, e2) -> e1.getValue().compareTo(e2.getValue())) // compare value of each counts (number of same cards)
                        .map(e -> e.getValue()); // Return the value (number of counts) of the max found occurrences

                Optional<Integer> maxKey = counts.entrySet().stream()
                        .filter(e -> e.getValue().equals(maxOccurrences.get()))
                        .max((e1, e2) -> e1.getKey().compareTo(e2.getKey()))
                        .map(e -> e.getKey());

                usersCardValues.put(player, new AbstractMap.SimpleEntry<>(maxKey.get(), maxOccurrences.get()));
            }

            Optional<Integer> maxPlayerOccurrences = usersCardValues.entrySet().stream()
                    .max((e1, e2) -> e1.getValue().getValue().compareTo(e2.getValue().getValue()))
                    .map(e -> e.getValue().getValue());


            Optional<Integer> maxPlayerKey = usersCardValues.entrySet().stream()
                    .filter(e -> e.getValue().getValue().equals(maxPlayerOccurrences.get()))
                    .max((e1, e2) -> e1.getValue().getKey().compareTo(e2.getValue().getKey()))
                    .map(e -> e.getValue().getKey());

            List<Player> gagnantProbable = new ArrayList<>();

            //Vérifie si plusieurs joueurs ont le même type de carte et d'occurences
            for (Player player : players) {

                Integer occurrences = usersCardValues.get(player).getValue();
                Integer cardValue = usersCardValues.get(player).getKey();

                if (cardValue.equals(maxPlayerKey.get()) && occurrences.equals(maxPlayerOccurrences.get())) {
                    gagnantProbable.add(player);
                }
            }
            // Les jetons sont enlevés de la main du joueur actuel
            List<Component> boardCoins = board.getSpecificComponents(Coin.class);

            for (Component coin : boardCoins) {
                coin.setPlayer(null);
            }

            System.out.println("-----------");
            for (Player player : players) {
                Integer occurrences = usersCardValues.get(player).getValue();
                Integer cardValue = usersCardValues.get(player).getKey();

                System.out.println("Player " + player.getName() + " has " + occurrences + " same card(s) of value " + cardValue);
            }
            System.out.println("-----------");
            for (Player player : gagnantProbable) {
                Integer occurrences = usersCardValues.get(player).getValue();
                Integer cardValue = usersCardValues.get(player).getKey();

                System.out.println("Player " + player.getName() + " won the game with " + occurrences + " same card(s) of value " + cardValue);
            }
            System.out.println("-----------");

            // CLes jetons sont dans la main du gagnant
            if (gagnantProbable.size() > 1) {

                System.out.println("Equality found between " + gagnantProbable.size() + " players. The gains will be randomly distributed");

                Map<Player, Integer> coinSum = new HashMap<>();

                // Partage les jetons de manière aléatoire
                for (Component coin : boardCoins) {

                    Random random = new Random();

                    int userIndex = random.nextInt(gagnantProbable.size() - 1);

                    Player selectedPlayer = gagnantProbable.get(userIndex);

                    coin.setPlayer(selectedPlayer);
                    selectedPlayer.addComponent(coin);

                    coinSum.merge(selectedPlayer, coin.getValue(), Integer::sum);
                }

                for (Map.Entry<Player, Integer> entry : coinSum.entrySet()) {

                    System.out.println("Player [" + entry.getKey().getName() + "] gains: " + entry.getValue());
                }

            } else {

                for (Component coin : boardCoins) {
                    Player selectedPlayer = gagnantProbable.get(0);

                    coin.setPlayer(selectedPlayer);
                    selectedPlayer.addComponent(coin);
                }
            }

            for (Player player : players) {
                player.clearHand();
            }

            board.clear();

            System.out.println("-----------");
            System.out.println("End of the round n°" + (numberOfRounds + 1) + " of " + maxRounds);
            this.displayState();

            numberOfRounds++;

        } while(!this.end());

        //Cherche le gagnant par rapport à la somme des jetons dans sa main
        Integer bestCoinSum = 0;

        for (Player player : players) {

            Integer coinSum = 0;

            List<Component> playerCoins = player.getSpecificComponents(Coin.class);

            for (Component coin : playerCoins) {
                coinSum += coin.getValue();
            }

            if (coinSum > bestCoinSum) {
                bestCoinSum = coinSum;
                gameWinner = player;
            }
        }
        return gameWinner;
    }

    @Override
    public Player run(Map<Player, Socket> playerSockets) throws IOException, ClassNotFoundException {
        for(Socket playerSocket : playerSockets.values()){
            ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
            playerOos.writeObject("Game start");
        }

        Player gameWinner = null;

        // Distribution de chaque jeton
        int playerIndex = 0;

        for (Coin coin : coins) {

            players.get(playerIndex).addComponent(coin);
            coin.setPlayer(players.get(playerIndex));

            playerIndex++;

            if (playerIndex >= players.size()) {
                playerIndex = 0;
            }
        }

        do {
            // Distribue des cartes à chaque joueur pour ce round
            Collections.shuffle(cards);

            // Donner 3 cartes à chaque joueur
            int cardIndex = 0;

            for (int i = 0; i < 3; i++) {

                for (Player player : players) {
                    player.addComponent(cards.get(cardIndex));
                    cards.get(cardIndex).setPlayer(player);
                    cardIndex++;
                }

            }

            // Envoi MAJ de l'état du jeu à chaque joueur

            for (Socket playerSocket : playerSockets.values()) {
                ObjectOutputStream playerGame = new ObjectOutputStream(playerSocket.getOutputStream());
                playerGame.writeObject(this);
            }

            for(Map.Entry<Player,Socket>entry : playerSockets.entrySet()){
                ObjectOutputStream playercards = new ObjectOutputStream(entry.getValue().getOutputStream());
                playercards.writeObject(entry.getKey().hand());
            }

            // Par rapport à l'état de la main des joueurs, les joueurs peuvent choisir un jeton à jouer
            for (Player player : players) {



                System.out.println("Waiting for " + player.getName() + " to play...");

                for(Map.Entry<Player,Socket>entry : playerSockets.entrySet()){
                    if(entry.getKey()!=player){
                        ObjectOutputStream Oos = new ObjectOutputStream(entry.getValue().getOutputStream());
                        Oos.writeObject("Waiting for " + player.getName() + " to play...");
                    }
                }

                player.play(playerSockets.get(player));

                Object answer;
                Component coin = null;

                do{
                    ObjectInputStream ois = new ObjectInputStream(playerSockets.get(player).getInputStream());
                    answer = ois.readObject();
                    if (answer instanceof Component)
                        coin = (Component) answer;
                }while(!(answer instanceof Component));
                ObjectOutputStream oos3 = new ObjectOutputStream(playerSockets.get(player).getOutputStream());
                oos3.writeObject("You played " + coin.getName());

                for(Component coin1 : player.getSpecificComponents(Coin.class)){
                    if(coin1.getId().equals(coin.getId())){
                        player.removeComponent(coin1);
                    }
                }

                board.addComponent(coin);

                System.out.println(player.getName() + " has played " + coin.getName() + " coin");
            }

            // Affichage de 3 cartes
            for (int i = 0; i < 3; i++) {
                board.addComponent(cards.get(cardIndex));
                cardIndex++;
            }

            //board.displayState();
            for(Map.Entry<Player,Socket>entry : playerSockets.entrySet()){
                ObjectOutputStream sendboard = new ObjectOutputStream(entry.getValue().getOutputStream());
                sendboard.writeObject(board);
            }

            // Par rapport à l'état du jeu et de la main des joueurs, les joueurs peuvent choisir un jeton à jouer
            for (Player player : players) {



                System.out.println("Waiting for " + player.getName() + " to play...");

                for(Map.Entry<Player,Socket>entry : playerSockets.entrySet()){
                    if(entry.getKey()!=player){
                        ObjectOutputStream Oos = new ObjectOutputStream(entry.getValue().getOutputStream());
                        Oos.writeObject("Waiting for " + player.getName() + " to play...");
                    }
                }

                player.play(playerSockets.get(player));

                Object answer;
                Component coin = null;

                do{
                    //Lit et affiche le message envoyé par le serveur
                    ObjectInputStream ois = new ObjectInputStream(playerSockets.get(player).getInputStream());
                    answer = ois.readObject();
                    if (answer instanceof Component)
                        coin = (Component) answer;
                }while(!(answer instanceof Component));
                ObjectOutputStream oos3 = new ObjectOutputStream(playerSockets.get(player).getOutputStream());
                oos3.writeObject("You played " + coin.getName());

                for(Component coin1 : player.getSpecificComponents(Coin.class)){
                    if(coin1.getId().equals(coin.getId())){
                        player.removeComponent(coin1);
                    }
                }

                board.addComponent(coin);

                System.out.println(player.getName() + " has played " + coin.getName() + " coin");
            }

            Map<Player, Map.Entry<Integer, Integer>> usersCardValues = new HashMap<>();

            for (Player player : players) {

                // Retrieve all cards
                List<Component> allCards = new ArrayList<>();
                allCards.addAll(player.getSpecificComponents(Card.class));
                allCards.addAll(board.getSpecificComponents(Card.class));

                // Store combinations of same card of current user
                // <cardValue, Occurrences>
                Map<Integer, Integer> counts = new HashMap<>();


                for (Component e : allCards) {
                    counts.merge(e.getValue(), 1, Integer::sum);
                }

                Optional<Integer> maxOccurrences = counts.entrySet().stream() // Get stream of all counts
                        .max((e1, e2) -> e1.getValue().compareTo(e2.getValue())) // compare value of each counts (number of same cards)
                        .map(e -> e.getValue()); // Return the value (number of counts) of the max found occurrences

                Optional<Integer> maxKey = counts.entrySet().stream()
                        .filter(e -> e.getValue().equals(maxOccurrences.get()))
                        .max((e1, e2) -> e1.getKey().compareTo(e2.getKey()))
                        .map(e -> e.getKey());

                usersCardValues.put(player, new AbstractMap.SimpleEntry<>(maxKey.get(), maxOccurrences.get()));
            }

            Optional<Integer> maxPlayerOccurrences = usersCardValues.entrySet().stream()
                    .max((e1, e2) -> e1.getValue().getValue().compareTo(e2.getValue().getValue()))
                    .map(e -> e.getValue().getValue());

            Optional<Integer> maxPlayerKey = usersCardValues.entrySet().stream()
                    .filter(e -> e.getValue().getValue().equals(maxPlayerOccurrences.get()))
                    .max((e1, e2) -> e1.getValue().getKey().compareTo(e2.getValue().getKey()))
                    .map(e -> e.getValue().getKey());

            List<Player> gagnantProbable = new ArrayList<>();
            
            //Vérifie si plusieurs joueurs ont le même type de cartes et d'occurences
            for (Player player : players) {

                Integer occurrences = usersCardValues.get(player).getValue();
                Integer cardValue = usersCardValues.get(player).getKey();

                if (cardValue.equals(maxPlayerKey.get()) && occurrences.equals(maxPlayerOccurrences.get())) {
                    gagnantProbable.add(player);
                }
            }

            // Les jetons sont enlevés de la main de l'ancien joueur
            List<Component> boardCoins = board.getSpecificComponents(Coin.class);

            for (Component coin : boardCoins) {
                coin.setPlayer(null);
            }

            System.out.println("-----------");
            for (Player player : players) {
                Integer occurrences = usersCardValues.get(player).getValue();
                Integer cardValue = usersCardValues.get(player).getKey();


                for(Map.Entry<Player,Socket>entry : playerSockets.entrySet()){
                    ObjectOutputStream sendWinner = new ObjectOutputStream(entry.getValue().getOutputStream());
                    sendWinner.writeObject("Player " + player.getName() + " has " + occurrences + " same card(s) of value " + cardValue);
                }
                System.out.println("Player " + player.getName() + " has " + occurrences + " same card(s) of value " + cardValue);
            }
            System.out.println("-----------");
            for (Player player : gagnantProbable) {
                Integer occurrences = usersCardValues.get(player).getValue();
                Integer cardValue = usersCardValues.get(player).getKey();

                for(Map.Entry<Player,Socket>entry : playerSockets.entrySet()){
                    ObjectOutputStream sendWinner = new ObjectOutputStream(entry.getValue().getOutputStream());
                    sendWinner.writeObject("Player " + player.getName() + " won the game with " + occurrences + " same card(s) of value " + cardValue);
                }
                System.out.println("Player " + player.getName() + " won the game with " + occurrences + " same card(s) of value " + cardValue);
            }
            System.out.println("-----------");

            // Les jetons vont dans la main du gagnant
            if (gagnantProbable.size() > 1) {
                for(Map.Entry<Player,Socket>entry : playerSockets.entrySet()){
                    ObjectOutputStream sendWinner = new ObjectOutputStream(entry.getValue().getOutputStream());
                    sendWinner.writeObject("Equality found between " + gagnantProbable.size() + " players. The gains will be randomly distributed");
                }

                System.out.println("Equality found between " + gagnantProbable.size() + " players. The gains will be randomly distributed");

                Map<Player, Integer> coinSum = new HashMap<>();

                // Partage des jetons au hasard entre les joueurs
                for (Component coin : boardCoins) {

                    Random random = new Random();

                    int userIndex = random.nextInt(gagnantProbable.size() - 1);

                    Player selectedPlayer = gagnantProbable.get(userIndex);

                    coin.setPlayer(selectedPlayer);
                    selectedPlayer.addComponent(coin);

                    coinSum.merge(selectedPlayer, coin.getValue(), Integer::sum);
                }

                for (Map.Entry<Player, Integer> entry : coinSum.entrySet()) {
                    for(Map.Entry<Player,Socket>user : playerSockets.entrySet()){
                        ObjectOutputStream sendWinner = new ObjectOutputStream(user.getValue().getOutputStream());
                        sendWinner.writeObject("Player [" + entry.getKey().getName() + "] gains: " + entry.getValue());
                    }

                    System.out.println("Player [" + entry.getKey().getName() + "] gains: " + entry.getValue());
                }

            } else {

                for (Component coin : boardCoins) {
                    Player selectedPlayer = gagnantProbable.get(0);

                    coin.setPlayer(selectedPlayer);
                    selectedPlayer.addComponent(coin);
                }
            }

            for (Player player : players) {
                player.clearHand();
            }

            board.clear();

            for(Map.Entry<Player,Socket>entry : playerSockets.entrySet()){
                ObjectOutputStream updateGame = new ObjectOutputStream(entry.getValue().getOutputStream());
                updateGame.writeObject("-----------\n" + "End of the round n°" + (numberOfRounds + 1) + " of " + maxRounds + "\n");
            }

            System.out.println("-----------");
            System.out.println("End of the round n°" + (numberOfRounds + 1) + " of " + maxRounds);
            this.displayState();

            numberOfRounds++;


        } while(!this.end());
        for(Map.Entry<Player,Socket>entry : playerSockets.entrySet()){
            ObjectOutputStream updateGame = new ObjectOutputStream(entry.getValue().getOutputStream());
            updateGame.writeObject(this);
        }

        //cherche le gagnant par rapport à la somme des jetons dans sa main
        Integer bestCoinSum = 0;

        for (Player player : players) {

            Integer coinSum = 0;

            List<Component> playerCoins = player.getSpecificComponents(Coin.class);

            for (Component coin : playerCoins) {
                coinSum += coin.getValue();
            }

            if (coinSum > bestCoinSum) {
                bestCoinSum = coinSum;
                gameWinner = player;
            }
        }
        return gameWinner;
    }

    @Override
    public boolean end() {
        return numberOfRounds >= maxRounds;
    }
}
