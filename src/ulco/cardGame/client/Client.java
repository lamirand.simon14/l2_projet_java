package ulco.cardGame.client;

import ulco.cardGame.common.games.components.Coin;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Board;
import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.common.interfaces.Player;
import ulco.cardGame.server.SocketServer;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    private static Game game;


    public static void main(String[] args) {

        // Current use full variables
        try {

            // Create a connection to the server socket on the server application
            InetAddress host = InetAddress.getLocalHost();
            Socket socket;

            Object answer;
            String username;

            Scanner scanner = new Scanner(System.in);

            String reponse;

            System.out.println("Please select your username");
            username = scanner.nextLine();

            socket = new Socket(host.getHostName(), SocketServer.PORT);

            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            oos.writeObject(username);

            do {

                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                answer = ois.readObject();


                // depending of object type, we can manage its data
                if (answer instanceof String){
                    System.out.println(answer.toString());
                    if(answer.toString().equals("Press enter to play")){
                        reponse = scanner.nextLine();
                        for(Player player : game.getPlayers()){
                            if(player.getName().equals(username)){
                                ObjectOutputStream oos2 = new ObjectOutputStream(socket.getOutputStream());
                                oos2.writeObject(player.play());
                            }
                        }

                    }
                    else{
                        if(answer.toString().equals("["+username+"], it's your turn")){
                            Component coinToPlay;
                            for(Player player : game.getPlayers()){
                                if(player.getName().equals(username)){
                                    coinToPlay = player.play();
                                    ObjectOutputStream oos2 = new ObjectOutputStream(socket.getOutputStream());
                                    oos2.writeObject(coinToPlay);
                                }
                            }
                        }
                    }
                }

                if (answer instanceof Game){
                    game = (Game) answer;
                    ((Game)answer).displayState();
                }
                if (answer instanceof Board)
                    ((Board)answer).displayState();


            } while (!answer.equals("END"));

            // close the socket instance connection
            socket.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
