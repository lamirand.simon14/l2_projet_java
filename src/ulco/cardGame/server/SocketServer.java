package ulco.cardGame.server;

import ulco.cardGame.common.games.CardGame;
import ulco.cardGame.common.games.PokerGame;
import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.common.interfaces.Player;
import ulco.cardGame.common.players.CardPlayer;
import ulco.cardGame.common.players.PokerPlayer;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Constructor;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Server Game management with use of Singleton instance creation
 */
public class SocketServer {

    private ServerSocket server;
    protected Game game; // game will be instantiate later
    protected Map<Player, Socket> playerSockets;
    protected Constructor playerConstructor;

    public static final int PORT = 7777;

    private SocketServer() {
        try {
            server = new ServerSocket(PORT);
            playerSockets = new HashMap<>();
            playerConstructor = null;

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        SocketServer server = new SocketServer();
        Scanner scanner = new Scanner(System.in);
        String game = "toto";
        while(!game.equals("Battle") && !game.equals("Poker")){
            System.out.println("Choose your game : Battle or Poker");
            game = scanner.nextLine();
        }
        server.run(game);
    }

    private void run(String gameName) {

        try {

            // Need to specify your current cardGame.txt file
            if(gameName.equals("Battle")){
                game = new CardGame("Battle", 3, "resources/games/cardGame.txt");
            }
            else{
                game = new PokerGame("Poker", 3, 4,"resources/games/pokerGame.txt");
            }

            // Game Loop
            System.out.println("Waiting for new player for " + game.toString());

            // add each player until game is ready (or number of max expected player is reached)
            // Waiting for the socket entrance

            do {

                Socket socket = server.accept();

                // read socket Data
                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                String playerUsername = (String) ois.readObject();

                // Create player instance
                Player player = null;
                if(gameName.equals("Battle")){
                    player = new CardPlayer(playerUsername);
                }
                else{
                    player = new PokerPlayer(playerUsername);
                }


                if(game.addPlayer(player,socket)){
                    ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                    oos.writeObject(game);

                    // Tell to other players that new player is connected
                    for (Socket playerSocket : playerSockets.values()) {

                        ObjectOutputStream playerOOS = new ObjectOutputStream(playerSocket.getOutputStream());
                        playerOOS.writeObject("Now connected: " + player.getName());
                    }

                    // Store player's socket in dictionary (Map)
                    playerSockets.put(player, socket);

                    System.out.println(player.getName() + " is now connected...");
                }
                else{
                    System.out.println("Error during the connection");
                }




            } while (!game.isStarted());

            // run the whole game using sockets
            Player gameWinner = game.run(playerSockets);
            for (Socket playerSocket : playerSockets.values()) {
                ObjectOutputStream playerOOS = new ObjectOutputStream(playerSocket.getOutputStream());
                playerOOS.writeObject("The winner is " + gameWinner.getName());
            }


            // Tells to player that server will be closed (just as example)
            for (Socket playerSocket : playerSockets.values()) {
                ObjectOutputStream playerOOS = new ObjectOutputStream(playerSocket.getOutputStream());
                playerOOS.writeObject("END");
            }

            // Close each socket when game is finished
            for (Socket socket : playerSockets.values()) {
                socket.close();
            }

            game.removePlayers();

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}

